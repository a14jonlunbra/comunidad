<div><?php echo $msg ?></div>

<div class="form">

<?php $form = $this->beginWidget('CActiveForm', array
	(
		'htmlOptions'=>array('enctype'=>'multipart/form-data'),
		'method'=>'POST',
		'action'=>Yii::app()->createUrl('comunidad/registro'),
		'id'=>'form',
		'enableAjaxValidation'=>true,
		'enableClientValidation'=>true,
		'clientOptions'=>array(
			'validateOnSubmit'=>true,
			'validateOnChange'=>true,
			'validateOnType'=>true,
		)
	)); 

?>

	<p class="note">Campos con <span class="required">*</span> son requeridos.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model, 'nombre'); ?>
		<?php echo $form->textField($model, 'nombre'); ?>
		<?php echo $form->error($model, 'nombre'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email'); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model, 'password'); ?>
		<?php echo $form->passwordField($model, 'password'); ?>
		<?php echo $form->error($model, 'password'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model, 'repetir_password'); ?>
		<?php echo $form->passwordField($model, 'repetir_password'); ?>
		<?php echo $form->error($model, 'repetir_password'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model, 'terminos', array('style'=>'display:inline')); ?>
		<?php echo $form->checkBox($model, 'terminos'); ?>
		<?php echo $form->error($model, 'terminos'); ?>
	</div>

	<?php if(CCaptcha::checkRequirements()): ?>
	<div class="row">
		<?php echo $form->labelEx($model,'captcha'); ?>
		<div>
			<?php $this->widget('CCaptcha'); ?>
			<?php echo $form->textField($model,'captcha'); ?>
		</div>
		<div class="hint">
			Por favor introduce las letras que ve en la imagen.
			<br/>Las letras no son case-sensitive.
		</div>
		<?php echo $form->error($model,'captcha'); ?>
	</div>
	<?php endif; ?>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Submit'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->