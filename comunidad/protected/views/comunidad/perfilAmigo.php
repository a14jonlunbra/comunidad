<div class="container ">

	<div class="notificaciones">
		<?php 
			if(Yii::app()->user->hasFlash('mensajebueno'))
			{	
				?>
				<div class="info" style="color:green">
					<h3><?php echo Yii::app()->user->getFlash('mensajebueno') ?></h3>
				</div>
				<?php
			}
			if(Yii::app()->user->hasFlash('mensajemalo'))
			{	
				?>
				<div class="info" style="color:red">
					<h3><?php echo Yii::app()->user->getFlash('mensajemalo') ?></h3>
				</div>
				<?php
			}
		?>	
	</div>

	<div class="imagen_perfil">
		<img class="desenfoque_imagen" src="<?php echo $fotoUsuario; ?>"/>
	</div>
	<div class="imagen_perfil">
		<img src="<?php echo $fotoUsuario; ?>"/>
	</div>
	<div class="row cabecera">
		<div class="col-md-3 col-md-offset-6 boton_cabecera_centrado">
			<?php 
				if(isset($peticionEnviada))
				{
					if($peticionEnviada == 3)
					{
						?>
						<div class="btn-group">
						  	<button onclick="window.location.href='/comunidad/index.php/comunidad/addFriend?data=<?php echo $nombreUsuario ?>'" type="button" class="btn btn-sm boton_pendiente">
						  		<span class="glyphicon glyphicon-user"></span>Solicitud enviada
						  	</button>
						</div>
						<?php
					}
						else if($peticionEnviada == 1)
						{
							?>
							<div class="btn-group">
							  	<button type="button" class="btn btn-sm btn-default">
							  		<span class="glyphicon glyphicon-ok"></span>Amigo
							  	</button>
							  	<button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							    	<span class="glyphicon glyphicon-chevron-down"></span>
							    	<span class="sr-only">Toggle Dropdown</span>
							  	</button>
							  	<ul class="dropdown-menu">
							    	<li><a href="#">Eliminar amigo</a></li>
							  	</ul>
							</div>
							<?php
						}
				}
					else if(isset($peticionSolicitada))
					{
						if($peticionSolicitada == 1)
						{
							?>
							<div class="btn-group">
								  <button type="button" class="btn btn-sm btn-default">
								  	<span class="glyphicon glyphicon-ok"></span>Amigo
								  </button>
								  <button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								   	<span class="glyphicon glyphicon-chevron-down"></span>
								   	<span class="sr-only">Toggle Dropdown</span>
								  </button>
								  <ul class="dropdown-menu">
								   	<li><a href="#">Eliminar amigo</a></li>
								  </ul>
							</div>
							<?php
						}
							else if($peticionSolicitada == 3)
							{
								?>
									<div class="btn-group">
									  	<button type="button" class="btn btn-sm boton_pendiente">
									  		<span class="glyphicon glyphicon-ok"></span>Te ha enviado una solicitud
									  	</button>
									</div>
								<?php				
							}
					}else
					{
						?>
						<div class="btn-group">
							<button onclick="window.location.href='/comunidad/index.php/comunidad/addFriend?data=<?php echo $nombreUsuario ?>'" type="button" class="btn btn-sm btn-default">
							  	<span class="glyphicon glyphicon-user"></span>Agregar amigo
							</button>
						</div>
						<?php
					}

			?>
		</div>
		<div class="col-md-3 boton_cabecera_centrado">
			<div class="btn-group">
			  <button type="button" class="btn btn-sm btn-default">
			  		<span class="glyphicon glyphicon-envelope"></span>Mensajes
			  </button>
			  <button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			    <span class="glyphicon glyphicon-chevron-down"></span>
			    <span class="sr-only">Toggle Dropdown</span>
			  </button>
			  <ul class="dropdown-menu">
			    <li><a href="#">Action</a></li>
			    <li><a href="#">Another action</a></li>
			    <li role="separator" class="divider"></li>
			    <li><a href="#">Separated link</a></li>
			  </ul>
			</div>
		</div>
	</div>

		<div class="row datos_usuario datos">
			<div class="col-md-4 col-md-offset-3 centrado bienvenido">
				<p>Usuario:</p> 
				<h3><?php echo $nombreUsuario ?></h3>
			</div>
			<?php 
			if(isset($totalNoAmigos))
			{
				if($totalNoAmigos == 0)
				{
			?>
					<div class="col-md-2 col-md-offset-3 centrado">
						<ul class="nav nav-pills" role="tablist">
						  	<li role="presentation"><a href="/comunidad/index.php/comunidad/amigosAmigo?nombreAmigo=<?php echo $nombreUsuario; ?>">Amigos<span class="badge"><?php echo $totalAmigos; ?></span></a></li>
						</ul>
					</div>
			<?php
				}else if($totalNoAmigos > 0)
				{
			?>
					<div class="col-md-2 col-md-offset-3 centrado">
						<ul class="nav nav-pills" role="tablist">
						  	<li role="presentation"><a href="/comunidad/index.php/comunidad/amigosAmigo">Amigos<span class="badge"><?php echo $totalAmigos; ?></span></a></li>
						</ul>
					</div>
			<?php
				}
			}
			?>
		</div>


</div>

<?php

Yii::app()->clientScript->registerScript(
      "test6",
      "$(document).ready(function(){
			$('.info').animate({opacity: 1.0}, 3000).fadeOut('slow');
      });
      ",
      CClientScript::POS_END
);

?>