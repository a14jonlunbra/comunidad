<div class="container ">

	<div class="notificaciones">
		<?php 
			if(Yii::app()->user->hasFlash('mensaje'))
			{	
				?>
				<div class="info" style="color:red">
					<h3><?php echo Yii::app()->user->getFlash('mensaje') ?></h3>
				</div>
				<?php
			}
		?>
		<?php 
			if(Yii::app()->user->hasFlash('success'))
			{	
				?>
				<div class="info" style="color:green">
					<h3><?php echo Yii::app()->user->getFlash('success') ?></h3>
				</div>
				<?php
			}else
			{
				?>
				<div class="info" style="color:red">
					<h3><?php echo Yii::app()->user->getFlash('error') ?></h3>
				</div>
				<?php
			}
		?>	
	</div>

	<div class="imagen_perfil">
		<img class="desenfoque_imagen" src="<?php echo $fotoUsuario; ?>"/>
		<?php $form = $this->beginWidget('CActiveForm', array
			(
				'htmlOptions'=>array('enctype'=>'multipart/form-data'),
				'htmlOptions'=>array('id'=>'enviarfotodirecta')
			)); 

		?>
				<p>
					<?php
						echo CHtml::activeFileField($model, 'foto');
					?>
				</p>

		<?php $this->endWidget(); ?>
	</div>
	<div class="cabecera">
	
	</div>

		<div class="row datos_usuario datos">
			<div class="col-md-4 col-md-offset-3 centrado bienvenido">
				<p>Bienveido:</p> 
				<h3><?php echo $nombreUsuario ?></h3>
			</div>
			<?php
			if(isset($totalNoAmigos))
			{
				if($totalNoAmigos == 0)
				{
			?>
					<div class="col-md-2 col-md-offset-3 centrado">
						<ul class="nav nav-pills" role="tablist">
						  	<li role="presentation"><a href="/comunidad/index.php/comunidad/amigos">Amigos<span class="badge"><?php echo $totalAmigos; ?></span></a></li>
						</ul>
					</div>
			<?php
				}else if($totalNoAmigos > 0)
				{
			?>
					<div class="col-md-4 col-md-offset-1 centrado">
						<ul class="nav nav-pills" role="tablist">
						  	<li role="presentation"><a href="/comunidad/index.php/comunidad/amigos">Amigos<span class="badge"><?php echo $totalAmigos; ?></span></a></li>
							<li role="presentation"><a href="/comunidad/index.php/comunidad/amigos">Peticiones Amistad<span class="badge_peticion_amistad"><?php echo $totalNoAmigos; ?></span></a></li>
						</ul>
					</div>
			<?php
				}
			}
			?>
		</div>
	
		<?php
		if(isset($activo3) || isset($activo1))
		{
			if($activo1 == 0 && $activo3 == 0)
			{
				?>
				<div class="row div_amigos">
					<div class="col-md-12">
						<?php
						echo "Todavia no tienes usuarios agregados";
						?>
					</div>
				</div>
				<?php
			}
				else if($activo3 > 0 && $activo1 > 0)
				{
					?>
					<div class="row div_amigos">
						<div class="col-md-7">
							<h3>Usuarios agregados</h3>
							<?php
							echo $agregados;
							?>
						</div>
						<div class="col-md-5">
							<h3>Usuarios pendientes</h3>
							<?php
							echo $pendientes;
							?>
						</div>
					</div>
					<?php
				}
					else if($activo3 == 0 && $activo1 > 0){
						?>
						<div class="row div_amigos">
							<div class="col-md-12">
								<h3>Usuarios agregados</h3>
								<?php
									echo $agregados;
								?>
							</div>
						</div>
						<?php
					}
						else if($activo3 > 0 && $activo1 == 0)
						{
							?>
							<div class="row div_amigos">
								<div class="col-md-7">
									<h3>Usuarios agregados</h3>
									<?php
									echo "Todavia no tienes usuarios agregados";
									?>
								</div>
								<div class="col-md-5">
									<h3>Usuarios pendientes</h3>
									<?php
									echo $pendientes;
									?>
								</div>
							</div>
							<?php
						}
		}
		?>

<?php

Yii::app()->clientScript->registerScript(
      "test3",
      "$(document).ready(function(){
      		$('.imagen_perfil').mouseover(function(){
      			$('.desenfoque_imagen').css({
      				 '-webkit-filter': 'opacity(20%)',
      				 '-moz-filter': 'opacity(20%)',
      				 'filter': 'opacity(20%)'
      			});
				$('#enviarfotodirecta').css({
      				 'display':'block'
      			});
      		});
			$('.imagen_perfil').mouseout(function(){
      			$('.desenfoque_imagen').css({
      				 '-webkit-filter': 'none',
      				 '-moz-filter': 'none',
      				 'filter': 'none'
      			});
				$('#enviarfotodirecta').css({
      				 'display':'none'
      			});
      		});	
			$('#enviarfotodirecta').mouseover(function(){
				$('#enviarfotodirecta').css({
      				 'display':'block'
      			});
				$('.desenfoque_imagen').css({
      				 '-webkit-filter': 'opacity(20%)',
      				 '-moz-filter': 'opacity(20%)',
      				 'filter': 'opacity(20%)'
      			});
      		});
			$('#enviarfotodirecta').mouseout(function(){
				$('#enviarfotodirecta').css({
      				 'display':'none'
      			});
				$('.desenfoque_imagen').css({
      				 '-webkit-filter': 'none',
      				 '-moz-filter': 'none',
      				 'filter': 'none'
      			});
      		});
			
			$('#Usuario_foto').change(function(){
				$('#enviarfotodirecta').submit();
			});
			$('#enviarfotodirecta').css({
				'position':'absolute',
				'top':'68px',
				'left':'24px',
				'z-index':'2',
				'display':'none'
			});

			$('.info').animate({opacity: 1.0}, 3000).fadeOut('slow');

      });
      ",
      CClientScript::POS_END
);

Yii::app()->clientScript->registerScript(
      "test4",
      "function eliminarUsuario(id){
			var r = confirm('Seguro que deseas eliminar a este usuario de tus amigos!');
		    if (r == true) 
		    {
				window.location.href = '/comunidad/index.php/comunidad/eliminarAmigo?data='+id;
		    } else {
		        alert('El usuario no se eliminara');
		    }
		}
      ",
      CClientScript::POS_END
);

?>