<div class="container ">

	<div class="notificaciones">
		<?php 
			if(Yii::app()->user->hasFlash('mensaje'))
			{	
				?>
				<div class="info" style="color:red">
					<h3><?php echo Yii::app()->user->getFlash('mensaje') ?></h3>
				</div>
				<?php
			}
		?>	
	</div>

	<div class="imagen_perfil">
		<img class="desenfoque_imagen" src="<?php echo $fotoUsuario; ?>"/>
		<?php $form = $this->beginWidget('CActiveForm', array
			(
				'htmlOptions'=>array('enctype'=>'multipart/form-data'),
				'htmlOptions'=>array('id'=>'enviarfotodirecta')
			)); 

		?>
				<p>
					<?php
						echo CHtml::activeFileField($model, 'foto');
					?>
				</p>

		<?php $this->endWidget(); ?>
	</div>
	<div class="cabecera">
	
	</div>

		<div class="row datos_usuario datos">
			<div class="col-md-4 col-md-offset-3 centrado bienvenido">
				<p>Bienveido:</p> 
				<h3><?php echo $nombreUsuario ?></h3>
			</div>
			<?php 
			if(isset($totalNoAmigos))
			{
				if($totalNoAmigos == 0)
				{
			?>
					<div class="col-md-2 col-md-offset-3 centrado">
						<ul class="nav nav-pills" role="tablist">
						  	<li role="presentation"><a href="/comunidad/index.php/comunidad/amigos">Amigos<span class="badge"><?php echo $totalAmigos; ?></span></a></li>
						</ul>
					</div>
			<?php
				}else if($totalNoAmigos > 0)
				{
			?>
					<div class="col-md-4 col-md-offset-1 centrado">
						<ul class="nav nav-pills" role="tablist">
						  	<li role="presentation"><a href="/comunidad/index.php/comunidad/amigos">Amigos<span class="badge"><?php echo $totalAmigos; ?></span></a></li>
							<li role="presentation"><a href="/comunidad/index.php/comunidad/amigos">Peticiones Amistad<span class="badge_peticion_amistad"><?php echo $totalNoAmigos; ?></span></a></li>
						</ul>
					</div>
			<?php
				}
			}
			?>
		</div>


</div>

<?php

Yii::app()->clientScript->registerScript(
      "test5",
      "$(document).ready(function(){
      		$('.imagen_perfil').mouseover(function(){
      			$('.desenfoque_imagen').css({
      				 '-webkit-filter': 'opacity(20%)',
      				 '-moz-filter': 'opacity(20%)',
      				 'filter': 'opacity(20%)'
      			});
				$('#enviarfotodirecta').css({
      				 'display':'block'
      			});
      		});
			$('.imagen_perfil').mouseout(function(){
      			$('.desenfoque_imagen').css({
      				 '-webkit-filter': 'none',
      				 '-moz-filter': 'none',
      				 'filter': 'none'
      			});
				$('#enviarfotodirecta').css({
      				 'display':'none'
      			});
      		});	
			$('#enviarfotodirecta').mouseover(function(){
				$('#enviarfotodirecta').css({
      				 'display':'block'
      			});
				$('.desenfoque_imagen').css({
      				 '-webkit-filter': 'opacity(20%)',
      				 '-moz-filter': 'opacity(20%)',
      				 'filter': 'opacity(20%)'
      			});
      		});
			$('#enviarfotodirecta').mouseout(function(){
				$('#enviarfotodirecta').css({
      				 'display':'none'
      			});
				$('.desenfoque_imagen').css({
      				 '-webkit-filter': 'none',
      				 '-moz-filter': 'none',
      				 'filter': 'none'
      			});
      		});
			
			$('#Usuario_foto').change(function(){
				$('#enviarfotodirecta').submit();
			});
			$('#enviarfotodirecta').css({
				'position':'absolute',
				'top':'68px',
				'left':'24px',
				'z-index':'2',
				'display':'none'
			});

			$('.info').animate({opacity: 1.0}, 3000).fadeOut('slow');
      });
      ",
      CClientScript::POS_END
);

?>