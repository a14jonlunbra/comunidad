<?php ?>
<!DOCTYPE html>
<html lang='es'>
<head>
	
	<meta http-equiv="Content-type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="language" content="es">
	
	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print">
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection">
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/bootstrap/dist/js/jquery.js">

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
	<link rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">

	<!-- fichero css3 externo /-->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/bootstrap/dist/css/index.css">

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>

	<?php
		$cs = Yii::app()->clientScript;
		$cs->scriptMap = array(
			'jquery.js' => Yii::app()->request->baseUrl.'/bootstrap/dist/js/jquery.js',
			'bootstrap.min.js' => Yii::app()->request->baseUrl.'/bootstrap/dist/js/bootstrap.min.js',
		);
		$cs->registerCoreScript('jquery');
	?>

</head>
<body class="backgr">

<div class="container-fluid">
	<!-- MENU -->
	<div class="col-xs-12 col-md-12 menu">
	 	<nav class = "navbar navbar-default" role = "navigation">
   
			<div class = "navbar-header">
				<button type = "button" class = "navbar-toggle collapsed" 
				data-toggle = "collapse" data-target = "#navbar_1">
				    <span class = "sr-only">Toggle navigation</span>
				    <span class = "icon-bar"></span>
				    <span class = "icon-bar"></span>
				    <span class = "icon-bar"></span>
				</button>
			</div>
				   
			<div class = "collapse navbar-collapse" id = "navbar_1">			
				<ul class = "nav navbar-nav navbar-left posicion_menu">
				    <li class="texto"><a href = "/comunidad/index.php/comunidad/perfil">Perfil</a></li>
				    <li class="texto"><a href = "/comunidad/index.php/comunidad/grupos">All Offers</a></li>
				    <li class="texto"><a href = "#">Profile</a></li>	
				    <li class="texto"><a href = "#">About</a></li>
				    <?php
				    	if(isset(Yii::app()->user->id))
				    	{
				    		if(Yii::app()->user->id != null)
				    		{
				    ?>
				    			<li class="texto"><a href = "/comunidad/index.php/site/logout">Logout</a></li>
				    <?php
				    		}
				    	}else{
				    ?>
				    		<li class="texto"><a href = "/comunidad/index.php/site/login">Login</a></li>

				    <?php
				    	}
				    ?>									    									
				</ul>
				<?php
					if(isset(Yii::app()->user->id))
					{
						if(Yii::app()->user->id != null)
						{
				?>
							<div class="webdesigntuts-workshop">
								<form class="navbar-form navbar-right" action="" method="">
									<input onkeyup="mostrarSugerencia(this.value)" type="search" placeholder="Buscar usuario...">
								</form>
								<div class="lista_usuarios">
									<div class="col-md-12 usuarios"></div>
								</div>
							</div>
				<?php
						}
					}
				?>
			</div>

		</nav>
	</div>
	<!-- FIN DE MENU -->
</div>

<!-- CONTENIDO DE LAS VISTAS -->
<?php echo $content; ?>
<!-- FIN CONTENIDO DE LAS VISTAS -->

<?php
	if(isset(Yii::app()->user->id))
	{
		if(Yii::app()->user->id != null)
		{
		?>
		<div class="row chat">
			<div class="col-md-2 chat_box">
				<div class="chat_head">Chatbox</div>
				<div class="chat_body">
					<!-- Se genera con ajax funcion cargarUsuarios() -->
				</div>
			</div>
			<!-- Se genera con ajax -->
		</div>
		<?php
		}
	} 
?>



<?php 

Yii::app()->clientScript->registerScript(
      "test20",
      "function mostrarSugerencia(nombre){
	      $.ajax({
	            type: 'POST',
	            url: 'buscarUsuario',
	            data: { data: nombre },
	            dataType: 'json',
	            success: function(respuesta){
	            	$('.lista_usuarios').css({
	            		'display':'block'
	            	});
					if(respuesta==''){
						$('.lista_usuarios').css({
		            		'display':'none'
		            	});
					}
	            	$('.usuarios').remove();
	            	
	            	var ul = $('<ul/>',{
		            		'class':'usuarios'
		            	}).appendTo('.lista_usuarios'); 
		            if(respuesta.length != null){
		            	for(var i=0; i<respuesta.length; i++)
		            	{
		            		var divUsuario = $('<li/>',{
		            			'class':'usuario'+i
		            		}).appendTo('.usuarios');
							$('.usuarios').addClass('imagenBusqueda');
							var array = respuesta[i].split(',');
							var img = $('<img/>',{
								'class':'foto_usuario'+i
							}).appendTo('.usuario'+i);
							$('.foto_usuario'+i).attr('src',array[0]);
							var nombre = $('<p/>',{
								'class':'nombre_usuario_despliegue'+i
							}).appendTo('.usuario'+i);
							var diva = $('<a/>',{
		            			'class':'link'+i,
		            			'href':'/comunidad/index.php/comunidad/perfilAmigo?idAmigo='+array[2]
		            		}).appendTo('.usuario'+i);
							$('.usuario'+i).addClass('margen_datosUsuario');						
							$('.nombre_usuario_despliegue'+i).css({
								'padding':'0px 10px 0px 10px',
								'float':'left',
								'margin-top':'9px'
							});
							$('.nombre_usuario_despliegue'+i).text(array[1]);
							$('.usuario'+i).prepend(img);
							$('.usuario'+i).append(nombre);
							$('.usuario'+i).append(diva);
							var icono = $('<span/>',{
								'class':'glyphicon glyphicon-user',
								'aria-hidden':'true'
							}).appendTo('.link'+i);
							$('.link'+i).prepend('Ver perfil');
							$('.link'+i).append(icono);
							$('.link'+i).addClass('ver_perfil');
						}	    
		            }          	   
	      		}
	      });
		}
      ",
      CClientScript::POS_END
);

Yii::app()->clientScript->registerScript(
      "test0",
      "$(document).ready(function(){
      		$('.chat_head').click(function(){
				$('.chat_body').slideToggle('slow');
			});	

			$('.info').animate({opacity: 1.0}, 3000).fadeOut('slow');

      });
      ",
      CClientScript::POS_END
);

Yii::app()->clientScript->registerScript(
      "test2",
      "function cargarUsuarios(){
      		$('.chat_body').html('');
      		$.ajax({
	            type: 'POST',
	            url: 'cargarContactos',
	            dataType: 'json',
	            success: function(respuesta){
	            	$('.chat_body').append(respuesta);            	
	      		}
	   		});
		}
		cargarUsuarios();
		setInterval(function(){
			cargarUsuarios();
		}, 120000);
      ",
      CClientScript::POS_END
);

Yii::app()->clientScript->registerScript(
      "test15",
      "
 	  var a;
      function cargar(id){
      		a = id;
      		var usuario = id;
      		$('.msg_box').remove();	
				$.ajax({
		            type: 'POST',
		            data: { data: id },
		            url: 'cargarChatUsuario',
		            dataType: 'json',
		            success: function(respuesta){
		            	$('.chat').append(respuesta);
		            	$('.msg_wrap').show();
						$('.msg_box').show();

						$('.msg_head').click(function(){
							$('.msg_wrap').slideToggle('slow');
						});	

						$('.close').click(function(){
							$('.msg_box').hide();
						});

						$('textarea').keypress(function(e){
							if(e.keyCode==13)
							{
								var msg = $(this).val();
								var nombre = $('.nombre').text();
								$.ajax({
						            type: 'POST',
						            data: { data: msg , nombre: nombre},
						            url: 'guardarMensaje',
						            dataType: 'json',
						            success: function(respuesta){
						            	alert(respuesta);
						      		}
						   		});
								$(this).val('');
								$('<div class=msg_b>'+msg+'</div>').insertBefore('.msg_insert');
								$('.msg_body').scrollTop($('.msg_body')[0].scrollHeight);					
							}
						});

						$('.msg_body').scrollTop($('.msg_body')[0].scrollHeight);
		      		}

		   		});

		}

		setInterval(function(){
			if ($('.msg_box').is(':hidden'))
			{
			   
			}
			else
			{
				cargar(a);			   
			}

		}, 60000);

      ",
      CClientScript::POS_END
);

?>



</body>
</html>