<?php ?>
<!DOCTYPE html>
<html lang='es'>
<head>
	
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="language" content="es">
	
	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print">
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection">
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/bootstrap/dist/js/jquery.js">
	
	<!-- fichero css3 externo /-->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/bootstrap/dist/css/index.css">

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
	<link rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>

	<?php
		$cs = Yii::app()->clientScript;
		$cs->scriptMap = array(
			'jquery.js' => Yii::app()->request->baseUrl.'/bootstrap/dist/js/jquery.js',
			'bootstrap.min.js' => Yii::app()->request->baseUrl.'/bootstrap/dist/js/bootstrap.min.js',
		);
		$cs->registerCoreScript('jquery');
	?>

</head>
<body>

<header>
	 	
	 <div class="row cabecera">
	 		
	 	<div class="col-xs-5 col-sm-2 col-md-1 col-md-offset-1 imagen_perfil">
	 		<div class="imagen">
	 			<img class="img" width="100px" height="100px" src="<?php echo Yii::app()->request->baseUrl; ?>/images/perfil.png">
	 		</div>
	 	</div>
	 	
	 	<div class="col-xs-7 col-sm-3 col-md-2 datos_perfil">
	 		<div class="datos">
	 			<p>nombreusuario</p>
	 			<p>dinero</p>
	 		</div>
	 	</div>

	 	<div class="col-xs-12 col-sm-5 col-md-4 juego">
	 		<p class="imagen_juego">imagen</p>
	 	</div>

	 	<div class="col-xs-12 col-sm-2 col-md-4 language_perfil">
	 		<div class="col-md-12 language">
	 				<p>Language</p>
	 		</div>
	 	</div>

	 </div>

</header>

<!-- MENU -->
	<div class="col-xs-12 col-md-12 menu">
	 	<nav class = "navbar navbar-inverse" role = "navigation">
   
			<div class = "navbar-header">
				<button type = "button" class = "navbar-toggle collapsed" 
				data-toggle = "collapse" data-target = "#navbar_1">
				    <span class = "sr-only">Toggle navigation</span>
				    <span class = "icon-bar"></span>
				    <span class = "icon-bar"></span>
				    <span class = "icon-bar"></span>
				</button>
						
			<!--<a class = "navbar-brand" href = "#">Steam menu</a>-->
			</div>
				   
			<div class = "collapse navbar-collapse" id = "navbar_1">
					
				<ul class = "nav navbar-nav">
				    <li class="perfil_menu"><img class="imagen_menu" width="70px" height="70px" src="<?php echo Yii::app()->request->baseUrl; ?>/images/perfil.png"></li>
				    <li class="datos_perfil_menu">
				       	<div class="datos_menu">
				 			<p>nombreusuario</p>
				 			<p>dinero</p>
				 		</div>
				    </li>
				    <li><a class="distancia_lateral_menu" href = "#">Profile</a></li>
				    <li><a href = "#">All Offers</a></li>
				    <li><a href = "#">Search Offers</a></li>	
				    <li><a href = "#">Profile</a></li>	
				    <li><a href = "#">About</a></li>
				    <!--<li><a class="distancia_lateral_menu_final" href = "#">Login</a></li>-->
				    <li class="datos_perfil_menu">
				        <div class="datos_language">
				 			<p class="language_padding">LANGUAGE</p>
				 		</div>
				    </li>													
				</ul>
			</div>
				   
		</nav>
	</div>

	<div class="container-fluid">
		<?php echo $content; ?>
	</div>

	<script src="<?php echo Yii::app()->request->baseUrl; ?>/bootstrap/dist/js/jquery.js"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/bootstrap/dist/js/bootstrap.min.js"></script>

	<?php 

	Yii::app()->clientScript->registerScript(
      "test0",
      "$(function(){

			var posicion = $('header').height();
			
			var anchura = $(window).width();

			var altura=0;



			$(window).scroll(function(){	
			
				altura = $(window).scrollTop();
				
				if(altura >= posicion && anchura>768){
					$('.menu').css({
						'position':'fixed',
						'top':'0',
						'z-index':'2'
					});

					$('.perfil_menu').css({
						'visibility':'visible'
					});

					$('.datos_perfil_menu').css({
						'visibility':'visible'
					});

					$('.datos_menu').css({
						'padding-left':'35px'
					});

					$('.distancia_lateral_menu').css({
						'margin-left': '40%'
					});

					$('.language_padding').css({
						'padding-left' : '40%'
					});

					$('.navbar .navbar-nav').css({
						'margin-right': '3%'
					});

				}

					else if (altura < posicion || anchura <= 768){
						$('.menu').css({												
							'z-index':'2',
							'position': 'relative'
						});

						$('.perfil_menu').css({
							'visibility':'hidden'
						});

						$('.datos_perfil_menu').css({
							'visibility':'hidden'
						});

						$('.distancia_lateral_menu').css({
							'margin-left': '0%'
						});

						$('.language_padding').css({
							'padding-left' : '0%'
						});

						$('.navbar .navbar-nav').css({
							'margin-right': '7%'
						});

					}

						/*else if(altura > posicion && anchura <= 768){
							$('.menu').css({
								'position':'fixed',
								'top':'0',
								'z-index':'2'
							});
						}*/

			});

		});
      ",
    CClientScript::POS_END
	);

	?>

</body>
</html>