<?php

class ComunidadController extends Controller
{

	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}
	
	public function actionIndex()
	{
		$this->render("index");
	}

	public function actionRegistro()
	{
		$model = new ValidarRegistro;
		$usuario = new Usuario;
		$msg = '';

		if(isset($_POST['ajax']) && $_POST['ajax'] === 'form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		if(isset($_POST['ValidarRegistro']))
		{	
	
			$model->attributes = $_POST['ValidarRegistro'];
			$codigo_verificacion = rand(10000, 99999);

			if(!$model->validate())
			{
				$this->redirect($this->createUrl('comunidad/registro'));
			}else
			{
				$usuario->nombre = $_POST['ValidarRegistro']['nombre'];
				$usuario->email = $_POST['ValidarRegistro']['email'];
				$usuario->password = sha1($_POST['ValidarRegistro']['password']);
				$usuario->save();

				//$mail = new EnviarEmail();
				$subject = utf8_decode('Confirmar cuenta');
				$message = utf8_decode('Para confirmar su cuenta vaya a la siguiente direccion...');
				$message .= utf8_decode("<a href='http://localhost/comunidad/index.php/comunidad/registro?email='".$model->email."&codigo_verificacion=".$codigo_verificacion."'>Confirmar cuenta</a>"); 

				$this->enviar(
						Yii::app()->params['adminEmail'],
						$usuario->email,
						$subject,
						$message
					);

				$msg = 'Enhorabuena, le hemos enviado un correo electronico';
				$msg .= 'a su cuenta de email para que confirme su registro';
				$model->unsetAttributes();
			}
		}

		$this->render('registro', array('model'=>$model,'msg'=>$msg));
	}

	public function actionPerfil()
	{
		$id = Yii::app()->user->id;
		$model = new Usuario;

		if($id!=null)
		{	
		
			if(isset($_POST['Usuario']))
			{	
				$id = Yii::app()->user->id;
				$model->imagen = $_POST['Usuario']['foto'];
				$file = $_POST['Usuario']['foto'];
				$nombreFoto = $file;

				$trozos = explode(".", $nombreFoto); 
				$extension = end($trozos); 

				if($extension=='jpg' || $extension=='jpeg')
				{
					$ruta = "/comunidad/uploads";
					$resultado = @move_uploaded_file($_POST["Usuario"]["foto"], $ruta);
				}else
				{
					Yii::app()->user->setFlash('mensaje','La foto debe ser solo JPG/JPEG');
					$this->refresh();
				}

				$nombreFoto = '/comunidad/images/'.$nombreFoto;

				$query = Yii::app()->db->createCommand()
						->update('Usuario',
						array('imagen'=>new CDbExpression(':imagen', array(':imagen'=>$nombreFoto))),
						'id=:id', array(':id'=>$id)
						);

				if($query){
					$this->redirect($this->createUrl('comunidad/perfil'));
				}

			}else
			{
				$fotoUsuario = Usuario::model()->findByPk($id)->imagen;
				$nombreUsuario = Usuario::model()->findByPk($id)->nombre;
				$totalAmigosAgregados = Amigo::model()->findAll('(id_usuario1='.$id.' || id_usuario2='.$id.') && activo=1');
				$totalAmigos = sizeof($totalAmigosAgregados);
				$totalAmigosNoAgregados = Amigo::model()->findAll('(id_usuario1='.$id.' || id_usuario2='.$id.') && activo=3');
				$totalNoAmigos = sizeof($totalAmigosNoAgregados);
				$this->render('perfil',array('fotoUsuario'=>$fotoUsuario,'nombreUsuario'=>$nombreUsuario,'model'=>$model,'totalAmigos'=>$totalAmigos,'totalNoAmigos'=>$totalNoAmigos));
			}

		}else
		{
			$this->redirect($this->createUrl('site/login'));
		}
	}

	public function actionPerfilAmigo()
	{
		$id = Yii::app()->user->id;
		$idAm = $_GET['idAmigo'];
		if($id!=null)
		{
			$amigos = Amigo::model()->findAll();
			if(sizeof($amigos)>0)
			{		
				if(null == Amigo::model()->find('id_usuario1='.$id.' && id_usuario2='.$idAm.' || id_usuario1='.$idAm.' && id_usuario2='.$id))
				{
					$fotoUsuario = Usuario::model()->findByPk($idAm)->imagen;
					$nombreUsuario = Usuario::model()->findByPk($idAm)->nombre;
					$totalAmigosAgregados = Amigo::model()->findAll('(id_usuario1='.$idAm.' || id_usuario2='.$idAm.') && activo=1');
					$totalAmigos = sizeof($totalAmigosAgregados);
					$totalAmigosNoAgregados = Amigo::model()->findAll('(id_usuario1='.$idAm.' || id_usuario2='.$idAm.') && activo=3');
					$totalNoAmigos = sizeof($totalAmigosNoAgregados);
					$this->render('perfilAmigo',array('fotoUsuario'=>$fotoUsuario,'nombreUsuario'=>$nombreUsuario,'totalAmigos'=>$totalAmigos,'totalNoAmigos'=>$totalNoAmigos));
				}else
				{
					$amigos = Amigo::model()->find('id_usuario1='.$id.' && id_usuario2='.$idAm.' || id_usuario1='.$idAm.' && id_usuario2='.$id);
					if($amigos->id_usuario1 == $id)
					{	
						$peticionEnviada = $amigos->activo;
						$fotoUsuario = Usuario::model()->findByPk($idAm)->imagen;
						$nombreUsuario = Usuario::model()->findByPk($idAm)->nombre;
						$totalAmigosAgregados = Amigo::model()->findAll('(id_usuario1='.$idAm.' || id_usuario2='.$idAm.') && activo=1');
						$totalAmigos = sizeof($totalAmigosAgregados);
						$totalAmigosNoAgregados = Amigo::model()->findAll('(id_usuario1='.$idAm.' || id_usuario2='.$idAm.') && activo=3');
						$totalNoAmigos = sizeof($totalAmigosNoAgregados);
						$this->render('perfilAmigo',array('id'=>$id,'fotoUsuario'=>$fotoUsuario,'nombreUsuario'=>$nombreUsuario,'peticionEnviada'=>$peticionEnviada,'amigos'=>$amigos,'totalAmigos'=>$totalAmigos,'totalNoAmigos'=>$totalNoAmigos));
					}
						else if($amigos->id_usuario2 == $id)
						{
							$peticionSolicitada = $amigos->activo;
							$fotoUsuario = Usuario::model()->findByPk($idAm)->imagen;
							$nombreUsuario = Usuario::model()->findByPk($idAm)->nombre;
							$totalAmigosAgregados = Amigo::model()->findAll('(id_usuario1='.$idAm.' || id_usuario2='.$idAm.') && activo=1');
							$totalAmigos = sizeof($totalAmigosAgregados);
							$totalAmigosNoAgregados = Amigo::model()->findAll('(id_usuario1='.$idAm.' || id_usuario2='.$idAm.') && activo=3');
							$totalNoAmigos = sizeof($totalAmigosNoAgregados);
							$this->render('perfilAmigo',array('id'=>$id,'fotoUsuario'=>$fotoUsuario,'nombreUsuario'=>$nombreUsuario,'peticionSolicitada'=>$peticionSolicitada,'amigos'=>$amigos,'totalAmigos'=>$totalAmigos,'totalNoAmigos'=>$totalNoAmigos));
						}					
				}
			}else
			{
				$fotoUsuario = Usuario::model()->findByPk($idAm)->imagen;
				$nombreUsuario = Usuario::model()->findByPk($idAm)->nombre;
				$totalAmigosAgregados = Amigo::model()->findAll('(id_usuario1='.$idAm.' || id_usuario2='.$idAm.') && activo=1');
				$totalAmigos = sizeof($totalAmigosAgregados);
				$totalAmigosNoAgregados = Amigo::model()->findAll('(id_usuario1='.$idAm.' || id_usuario2='.$idAm.') && activo=3');
				$totalNoAmigos = sizeof($totalAmigosNoAgregados);
				$this->render('perfilAmigo',array('fotoUsuario'=>$fotoUsuario,'nombreUsuario'=>$nombreUsuario,'totalAmigos'=>$totalAmigos,'totalNoAmigos'=>$totalNoAmigos));
			}
		}else
		{
			$this->redirect($this->createUrl('site/login'));
		}
	}

	public function actionBuscarUsuario()
	{
		$id = Yii::app()->user->id;

		$sugerencia[] = array();

		$arrayNombres = Usuario::model()->findAll();

		foreach ($arrayNombres as $key => $value) {
			$nombres[] = $value->nombre;
		}

		if(isset($_POST['data']))
		{
			$datosRecibidos = $_POST['data'];
			$j = 0;
			if($datosRecibidos!=''){
				for($i=0; $i<count($nombres); $i++) 
				{
					if(strtolower($datosRecibidos) == strtolower(substr($nombres[$i], 0, strlen($datosRecibidos))))
					{
						$ide = Usuario::model()->findByAttributes(array('nombre'=>$nombres[$i]));
						
						if($ide->id!=$id)
						{
							$sugerencia[$j] = $ide->imagen.','.$nombres[$i].','.$ide->id;
							$j++;
						}
					}
				}
			}

			if($sugerencia == null || $datosRecibidos == '')
			{
				$sugerencia = "";
				echo json_encode($sugerencia);
			}else
			{
				echo json_encode($sugerencia);
			}
		}
	}

	/*public function actionAddFoto()
	{
		$model = new Usuario;

		if(isset($_POST['Usuario']))
		{
			$id = Yii::app()->user->id;
			$model->imagen = $_POST['Usuario'];
			$file = CUploadedFile::getInstance($model,'foto');
			$nombreFoto = $file->getName();

			if($file->getExtensionName()=='jpg' || $file->getExtensionName()=='jpeg')
			{
				$file->saveAs(Yii::getPathOfAlias('webroot').'/images/'.$file->getName());
			}else
			{
				Yii::app()->user->setFlash('mensaje','La foto debe ser solo JPG/JPEG');
				$this->refresh();
			}

			$nombreFoto = '/comunidad/images/'.$file->getName();

			$query = Yii::app()->db->createCommand()
					->update('Usuario',
					array('imagen'=>new CDbExpression(':imagen', array(':imagen'=>$nombreFoto))),
					'id=:id', array(':id'=>$id)
					);

			if($query){
				$this->redirect($this->createUrl('comunidad/perfil'));
			}

		}

		$this->render('addFoto', array('model'=>$model));
	}*/

	public function actionAddFriend()
	{
		$id = Yii::app()->user->id;
		$modelUser = new Amigo;
		$idAmi = Usuario::model()->findByAttributes(array('nombre'=>$_GET['data']));
		$idAm = $idAmi->id;
		$entra = 'true';
		$variable = array();
		if(isset($_GET['data']))
		{
			$id = Yii::app()->user->id;
			$nombre = $_GET['data'];
			$idAmigo = Usuario::model()->findByAttributes(array('nombre'=>$nombre));
			if($id!=$idAmigo->id)
			{
				$amigos =  Amigo::model()->findAll('id_usuario1='.$id.' || id_usuario2='.$id);
				if(sizeof($amigos)==0)
				{
					$modelUser->id_usuario1 = $id;
					$modelUser->id_usuario2 = $idAmigo->id;
					$modelUser->activo = 3;
					$modelUser->save();
					Yii::app()->user->setFlash('mensajebueno','Se ha mandado la peticion de amistad');
					$this->refresh();
				}

					else if(sizeof($amigos)>0)
					{
						foreach ($amigos as $key => $value) 
						{
							if(($value->id_usuario1 == $id && $value->id_usuario2 == $idAmigo->id) || ($value->id_usuario1 == $idAmigo->id && $value->id_usuario2 == $id))
							{
								$variable['mensajemalo'] = "El usuario ya es tu amigo";
								$entra = 'false';
							}
						}
						if($entra == 'true')
						{
							$modelUser = new Amigo;
							$modelUser->id_usuario1 = $id;
							$modelUser->id_usuario2 = $idAmigo->id;
							$modelUser->activo = 3;
							$modelUser->save();
							Yii::app()->user->setFlash('mensajebueno','Se ha mandado la peticion de amistad');
							$this->refresh();
						}
					}
			}

			$amigos = Amigo::model()->findAll();
			if(sizeof($amigos)>0)
			{		
				if(null == Amigo::model()->find('id_usuario1='.$id.' && id_usuario2='.$idAm.' || id_usuario1='.$idAm.' && id_usuario2='.$id))
				{
					$fotoUsuario = Usuario::model()->findByPk($idAm)->imagen;
					$nombreUsuario = Usuario::model()->findByPk($idAm)->nombre;
					$totalAmigosAgregados = Amigo::model()->findAll('(id_usuario1='.$idAm.' || id_usuario2='.$idAm.') && activo=1');
					$totalAmigos = sizeof($totalAmigosAgregados);
					$totalAmigosNoAgregados = Amigo::model()->findAll('(id_usuario1='.$idAm.' || id_usuario2='.$idAm.') && activo=3');
					$totalNoAmigos = sizeof($totalAmigosNoAgregados);
					$this->render('perfilAmigo',array('fotoUsuario'=>$fotoUsuario,'nombreUsuario'=>$nombreUsuario,'totalAmigos'=>$totalAmigos,'totalNoAmigos'=>$totalNoAmigos));
				}else
				{
					$amigos = Amigo::model()->find('id_usuario1='.$id.' && id_usuario2='.$idAm.' || id_usuario1='.$idAm.' && id_usuario2='.$id);
					if($amigos->id_usuario1 == $id)
					{	
						$peticionEnviada = $amigos->activo;
						$fotoUsuario = Usuario::model()->findByPk($idAm)->imagen;
						$nombreUsuario = Usuario::model()->findByPk($idAm)->nombre;
						$totalAmigosAgregados = Amigo::model()->findAll('(id_usuario1='.$idAm.' || id_usuario2='.$idAm.') && activo=1');
						$totalAmigos = sizeof($totalAmigosAgregados);
						$totalAmigosNoAgregados = Amigo::model()->findAll('(id_usuario1='.$idAm.' || id_usuario2='.$idAm.') && activo=3');
						$totalNoAmigos = sizeof($totalAmigosNoAgregados);
						$this->render('perfilAmigo',array('id'=>$id,'fotoUsuario'=>$fotoUsuario,'nombreUsuario'=>$nombreUsuario,'peticionEnviada'=>$peticionEnviada,'amigos'=>$amigos,'totalAmigos'=>$totalAmigos,'totalNoAmigos'=>$totalNoAmigos));
					}
						else if($amigos->id_usuario2 == $id)
						{
							$peticionSolicitada = $amigos->activo;
							$fotoUsuario = Usuario::model()->findByPk($idAm)->imagen;
							$nombreUsuario = Usuario::model()->findByPk($idAm)->nombre;
							$totalAmigosAgregados = Amigo::model()->findAll('(id_usuario1='.$idAm.' || id_usuario2='.$idAm.') && activo=1');
							$totalAmigos = sizeof($totalAmigosAgregados);
							$totalAmigosNoAgregados = Amigo::model()->findAll('(id_usuario1='.$idAm.' || id_usuario2='.$idAm.') && activo=3');
							$totalNoAmigos = sizeof($totalAmigosNoAgregados);
							$this->render('perfilAmigo',array('id'=>$id,'fotoUsuario'=>$fotoUsuario,'nombreUsuario'=>$nombreUsuario,'peticionSolicitada'=>$peticionSolicitada,'amigos'=>$amigos,'totalAmigos'=>$totalAmigos,'totalNoAmigos'=>$totalNoAmigos));
						}					
				}
			}else
			{
				$fotoUsuario = Usuario::model()->findByPk($idAm)->imagen;
				$nombreUsuario = Usuario::model()->findByPk($idAm)->nombre;
				$this->render('perfilAmigo',array('fotoUsuario'=>$fotoUsuario,'nombreUsuario'=>$nombreUsuario));
			}

		}

	}

	public function actionAmigosAmigo()
	{
		$nombre = $_GET['nombreAmigo'];
		$usuario = Usuario::model()->find('nombre="'.$nombre.'"');
		$id = $usuario->id;

		if($nombre!=null)
		{
			$fotoUsuario = Usuario::model()->findByPk($id)->imagen;
				$nombreUsuario = Usuario::model()->findByPk($id)->nombre;
				$amigosUsuario = Amigo::model()->findAll('id_usuario1='.$id.' || id_usuario2='.$id);
				$totalAmigosAgregados = Amigo::model()->findAll('(id_usuario1='.$id.' || id_usuario2='.$id.') && activo=1');
				$totalAmigos = sizeof($totalAmigosAgregados);
				$totalAmigosNoAgregados = Amigo::model()->findAll('(id_usuario1='.$id.' || id_usuario2='.$id.') && activo=3');
				$totalNoAmigos = sizeof($totalAmigosNoAgregados);

				if($amigosUsuario != null)
				{
					$agregados = '<ul>';
					$pendientes = '<ul>';
					$activo1 = 0;
					$activo3 = 0;
					$linkPerfil = "/comunidad/index.php/comunidad/perfilAmigo?idAmigo=";
					$linkAcetpar = "/comunidad/index.php/comunidad/aceptarUsuario?nombreUsuario=";
					$linkEliminar = "/comunidad/index.php/comunidad/eliminarUsuario?data=";

					foreach ($amigosUsuario as $key => $value) 
					{
						if($value->id_usuario1 == $id)
						{
							if($value->activo == 3)
							{
								$activo3 = $activo3+1; 
								$amigo = Usuario::model()->findByPk($value->id_usuario2);
								$pendientes = $pendientes.
								'<li>
									<img src="'.$amigo->imagen.'" width="50px" height="50px">
									<span>'.$amigo->nombre.'</span>'.
									'<button onclick="window.location.href='."'".$linkPerfil.$amigo->id."'".'" type="button" class="btn btn-default" aria-label="Left Align">
										<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
									</button>'.
									'<button onclick="window.location.href='."'".$linkAcetpar.$amigo->nombre."'".'" type="button" class="btn btn-default" aria-label="Left Align">
										<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
									</button>'.
									'<button onclick="window.location.href='."'".$linkEliminar.$amigo->id."'".'" type="button" class="btn btn-default" aria-label="Left Align">
										<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
									</button>
								</li>';
							}else
							{
								$activo1 = $activo1+1;
								$amigo = Usuario::model()->findByPk($value->id_usuario2);
								$agregados = $agregados.
								'<li class="col-md-6 lista_amigos">
									<img src="'.$amigo->imagen.'" width="50px" height="50px">
									<span>'.$amigo->nombre.'</span>'.
									'<button onclick="window.location.href='."'".$linkPerfil.$amigo->id."'".'" type="button" class="btn btn-default" aria-label="Left Align">
										<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
									</button>'.
									'<button onclick="window.location.href='."'".$linkAcetpar.$amigo->nombre."'".'" type="button" class="btn btn-default" aria-label="Left Align">
										<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
									</button>'.
									'<button onclick="window.location.href='."'".$linkEliminar.$amigo->id."'".'" type="button" class="btn btn-default" aria-label="Left Align">
										<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
									</button>
								</li>';
							}
						}
							else if($value->id_usuario2 == $id)
							{
								if($value->activo == 3)
								{
									$activo3 = $activo3+1; 
									$amigo = Usuario::model()->findByPk($value->id_usuario1);
									$pendientes = $pendientes.
									'<li>
										<img src="'.$amigo->imagen.'" width="50px" height="50px">
										<span>'.$amigo->nombre.'</span>'.
										'<button onclick="window.location.href='."'".$linkPerfil.$amigo->id."'".'" type="button" class="btn btn-default" aria-label="Left Align">
											<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
										</button>'.
										'<button onclick="window.location.href='."'".$linkAcetpar.$amigo->nombre."'".'" type="button" class="btn btn-default" aria-label="Left Align">
											<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
										</button>'.
										'<button onclick="window.location.href='."'".$linkEliminar.$amigo->id."'".'" type="button" class="btn btn-default" aria-label="Left Align">
											<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
										</button>
									</li>';
								}else
								{
									$activo1 = $activo1+1;
									$amigo = Usuario::model()->findByPk($value->id_usuario1);
									$agregados = $agregados.
									'<li class="col-md-6 lista_amigos">
										<img src="'.$amigo->imagen.'" width="50px" height="50px">
										<span>'.$amigo->nombre.'</span>'.
										'<button onclick="window.location.href='."'".$linkPerfil.$amigo->id."'".'" type="button" class="btn btn-default" aria-label="Left Align">
											<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
										</button>'.
										'<button onclick="window.location.href='."'".$linkAcetpar.$amigo->nombre."'".'" type="button" class="btn btn-default" aria-label="Left Align">
											<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
										</button>'.
										'<button onclick="window.location.href='."'".$linkEliminar.$amigo->id."'".'" type="button" class="btn btn-default" aria-label="Left Align">
											<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
										</button>
									</li>';
								}
							}
					}

					$agregados = $agregados.'</ul>';
					$pendientes = $pendientes.'</ul>';

					$this->render('amigosAmigo',array('fotoUsuario'=>$fotoUsuario,'nombreUsuario'=>$nombreUsuario,'agregados'=>$agregados,'pendientes'=>$pendientes,'id'=>$id,'totalAmigos'=>$totalAmigos,'totalNoAmigos'=>$totalNoAmigos,'activo1'=>$activo1,'activo3'=>$activo3));
				}else
				{
					$activo1 = 0;
					$activo3 = 0;
					$this->render('amigosAmigo',array('fotoUsuario'=>$fotoUsuario,'nombreUsuario'=>$nombreUsuario,'totalAmigos'=>$totalAmigos,'totalNoAmigos'=>$totalNoAmigos,'activo1'=>$activo1,'activo3'=>$activo3));
				}
		}else
		{
			$this->redirect($this->createUrl('site/login'));
		}
	}

	public function actionAmigos()
	{
		$id = Yii::app()->user->id;
		$model = new Usuario;

		if($id!=null)
		{	
		
			if(isset($_POST['Usuario']))
			{	
				$id = Yii::app()->user->id;
				$model->imagen = $_POST['Usuario']['foto'];
				$file = $_POST['Usuario']['foto'];
				$nombreFoto = $file;

				$trozos = explode(".", $nombreFoto); 
				$extension = end($trozos); 

				if($extension=='jpg' || $extension=='jpeg')
				{
					$ruta = "/comunidad/uploads";
					$resultado = @move_uploaded_file($_POST["Usuario"]["foto"], $ruta);
				}else
				{
					Yii::app()->user->setFlash('mensaje','La foto debe ser solo JPG/JPEG');
					$this->refresh();
				}

				$nombreFoto = '/comunidad/images/'.$nombreFoto;

				$query = Yii::app()->db->createCommand()
						->update('Usuario',
						array('imagen'=>new CDbExpression(':imagen', array(':imagen'=>$nombreFoto))),
						'id=:id', array(':id'=>$id)
						);

				if($query){
					$this->redirect($this->createUrl('comunidad/perfil'));
				}

			}else
			{	
				$fotoUsuario = Usuario::model()->findByPk($id)->imagen;
				$nombreUsuario = Usuario::model()->findByPk($id)->nombre;
				$amigosUsuario = Amigo::model()->findAll('id_usuario1='.$id.' || id_usuario2='.$id);
				$totalAmigosAgregados = Amigo::model()->findAll('(id_usuario1='.$id.' || id_usuario2='.$id.') && activo=1');
				$totalAmigos = sizeof($totalAmigosAgregados);
				$totalAmigosNoAgregados = Amigo::model()->findAll('(id_usuario1='.$id.' || id_usuario2='.$id.') && activo=3');
				$totalNoAmigos = sizeof($totalAmigosNoAgregados);

				if($amigosUsuario != null)
				{
					$agregados = '<ul>';
					$pendientes = '<ul>';
					$activo1 = 0;
					$activo3 = 0;
					$linkPerfil = "/comunidad/index.php/comunidad/perfilAmigo?idAmigo=";
					$linkAcetpar = "/comunidad/index.php/comunidad/aceptarUsuario?nombreUsuario=";
					$linkEliminar = "/comunidad/index.php/comunidad/eliminarUsuario?data=";

					foreach ($amigosUsuario as $key => $value) 
					{
						if($value->id_usuario1 == $id)
						{
							if($value->activo == 3)
							{
								$activo3 = $activo3+1; 
								$amigo = Usuario::model()->findByPk($value->id_usuario2);
								$pendientes = $pendientes.
								'<li>
									<img src="'.$amigo->imagen.'" width="50px" height="50px">
									<span>'.$amigo->nombre.'</span>'.
									'<button onclick="window.location.href='."'".$linkPerfil.$amigo->id."'".'" type="button" class="btn btn-default" aria-label="Left Align">
										<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
									</button>'.
									'<button onclick="window.location.href='."'".$linkAcetpar.$amigo->nombre."'".'" type="button" class="btn btn-default" aria-label="Left Align">
										<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
									</button>'.
									'<button onclick="window.location.href='."'".$linkEliminar.$amigo->id."'".'" type="button" class="btn btn-default" aria-label="Left Align">
										<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
									</button>
								</li>';
							}else
							{
								$activo1 = $activo1+1;
								$amigo = Usuario::model()->findByPk($value->id_usuario2);
								$agregados = $agregados.
								'<li class="col-md-6 lista_amigos">
									<img src="'.$amigo->imagen.'" width="50px" height="50px">
									<span>'.$amigo->nombre.'</span>'.'
									<button onclick="window.location.href='."'".$linkPerfil.$amigo->id."'".'" type="button" class="btn btn-default" aria-label="Left Align">
										<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
									</button>'.
									
									'<button onclick="window.location.href='."'".$linkEliminar.$amigo->id."'".'" type="button" class="btn btn-default" aria-label="Left Align">
										<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
									</button>
								</li>';
							}
						}
							else if($value->id_usuario2 == $id)
							{
								if($value->activo == 3)
								{
									$activo3 = $activo3+1; 
									$amigo = Usuario::model()->findByPk($value->id_usuario1);
									$pendientes = $pendientes.
									'<li>
										<img src="'.$amigo->imagen.'" width="50px" height="50px">
										<span>'.$amigo->nombre.'</span>'.
										'<button onclick="window.location.href='."'".$linkPerfil.$amigo->id."'".'" type="button" class="btn btn-default" aria-label="Left Align">
											<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
										</button>'.
										'<button onclick="window.location.href='."'".$linkAcetpar.$amigo->nombre."'".'" type="button" class="btn btn-default" aria-label="Left Align">
											<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
										</button>'.
										'<button onclick="window.location.href='."'".$linkEliminar.$amigo->id."'".'" type="button" class="btn btn-default" aria-label="Left Align">
											<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
										</button>
									</li>';
								}else
								{
									$activo1 = $activo1+1;
									$amigo = Usuario::model()->findByPk($value->id_usuario1);
									$agregados = $agregados.
									'<li class="col-md-6 lista_amigos">
										<img src="'.$amigo->imagen.'" width="50px" height="50px">
										<span>'.$amigo->nombre.'</span>'.
										'<button onclick="window.location.href='."'".$linkPerfil.$amigo->id."'".'" type="button" class="btn btn-default" aria-label="Left Align">
											<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
										</button>'.
										
										'<button onclick="window.location.href='."'".$linkEliminar.$amigo->id."'".'" type="button" class="btn btn-default" aria-label="Left Align">
											<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
										</button>
									</li>';
								}
							}
					}

					$agregados = $agregados.'</ul>';
					$pendientes = $pendientes.'</ul>';

					$this->render('amigos',array('fotoUsuario'=>$fotoUsuario,'nombreUsuario'=>$nombreUsuario,'model'=>$model,'agregados'=>$agregados,'pendientes'=>$pendientes,'id'=>$id,'totalAmigos'=>$totalAmigos,'totalNoAmigos'=>$totalNoAmigos,'activo1'=>$activo1,'activo3'=>$activo3));
				}else
				{
					$activo1 = 0;
					$activo3 = 0;
					$this->render('amigos',array('fotoUsuario'=>$fotoUsuario,'nombreUsuario'=>$nombreUsuario,'model'=>$model,'totalAmigos'=>$totalAmigos,'totalNoAmigos'=>$totalNoAmigos,'activo1'=>$activo1,'activo3'=>$activo3));
				}
			}

		}else
		{
			$this->redirect($this->createUrl('site/login'));
		}
	}

	public function actionAceptarUsuario()
	{
		$id = Yii::app()->user->id;
		$model = new Usuario;

		if($id!=null)
		{	
		
			if(isset($_POST['Usuario']))
			{	
				$id = Yii::app()->user->id;
				$model->imagen = $_POST['Usuario']['foto'];
				$file = $_POST['Usuario']['foto'];
				$nombreFoto = $file;

				$trozos = explode(".", $nombreFoto); 
				$extension = end($trozos); 

				if($extension=='jpg' || $extension=='jpeg')
				{
					$ruta = "/comunidad/uploads";
					$resultado = @move_uploaded_file($_POST["Usuario"]["foto"], $ruta);
				}else
				{
					Yii::app()->user->setFlash('mensaje','La foto debe ser solo JPG/JPEG');
					$this->refresh();
				}

				$nombreFoto = '/comunidad/images/'.$nombreFoto;

				$query = Yii::app()->db->createCommand()
						->update('Usuario',
						array('imagen'=>new CDbExpression(':imagen', array(':imagen'=>$nombreFoto))),
						'id=:id', array(':id'=>$id)
						);

				if($query){
					$this->redirect($this->createUrl('comunidad/perfil'));
				}

			}else
			{
				$nombre = $_GET['nombreUsuario'];
				$nombreAmigo = Usuario::model()->findByAttributes(array('nombre'=>$nombre));
				$idAmigo = Amigo::model()->find('id_usuario1='.$id.' && id_usuario2='.$nombreAmigo->id.' || id_usuario1='.$nombreAmigo->id.' && id_usuario2='.$id);
				$idAmigo->activo = 1;
				$idAmigo->update();

				$fotoUsuario = Usuario::model()->findByPk($id)->imagen;
				$nombreUsuario = Usuario::model()->findByPk($id)->nombre;
				$amigosUsuario = Amigo::model()->findAll('id_usuario1='.$id.' || id_usuario2='.$id);
				$totalAmigosAgregados = Amigo::model()->findAll('(id_usuario1='.$id.' || id_usuario2='.$id.') && activo=1');
				$totalAmigos = sizeof($totalAmigosAgregados);
				$totalAmigosNoAgregados = Amigo::model()->findAll('(id_usuario1='.$id.' || id_usuario2='.$id.') && activo=3');
				$totalNoAmigos = sizeof($totalAmigosNoAgregados);

				if($amigosUsuario != null)
				{
					$agregados = '<ul>';
					$pendientes = '<ul>';
					$activo1 = 0;
					$activo3 = 0;
					$linkPerfil = "/comunidad/index.php/comunidad/perfilAmigo?idAmigo=";
					$linkAcetpar = "/comunidad/index.php/comunidad/aceptarUsuario?nombreUsuario=";
					$linkEliminar = "/comunidad/index.php/comunidad/eliminarUsuario?data=";

					foreach ($amigosUsuario as $key => $value) 
					{
						if($value->id_usuario1 == $id)
						{
							if($value->activo == 3)
							{
								$activo3 = $activo3+1; 
								$amigo = Usuario::model()->findByPk($value->id_usuario2);
								$pendientes = $pendientes.
								'<li>
									<img src="'.$amigo->imagen.'" width="50px" height="50px">
									<span>'.$amigo->nombre.'</span>'.
									'<button onclick="window.location.href='."'".$linkPerfil.$amigo->id."'".'" type="button" class="btn btn-default" aria-label="Left Align">
										<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
									</button>'.
									'<button onclick="window.location.href='."'".$linkAcetpar.$amigo->nombre."'".'" type="button" class="btn btn-default" aria-label="Left Align">
										<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
									</button>'.
									'<button onclick="window.location.href='."'".$linkEliminar.$amigo->id."'".'" type="button" class="btn btn-default" aria-label="Left Align">
										<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
									</button>
								</li>';
							}else
							{
								$activo1 = $activo1+1;
								$amigo = Usuario::model()->findByPk($value->id_usuario2);
								$agregados = $agregados.
								'<li class="col-md-6 lista_amigos">
									<img src="'.$amigo->imagen.'" width="50px" height="50px">
									<span>'.$amigo->nombre.'</span>'.
									'<button onclick="window.location.href='."'".$linkPerfil.$amigo->id."'".'" type="button" class="btn btn-default" aria-label="Left Align">
										<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
									</button>'.
									'<button onclick="window.location.href='."'".$linkAcetpar.$amigo->nombre."'".'" type="button" class="btn btn-default" aria-label="Left Align">
										<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
									</button>'.
									'<button onclick="window.location.href='."'".$linkEliminar.$amigo->id."'".'" type="button" class="btn btn-default" aria-label="Left Align">
										<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
									</button>
								</li>';
							}
						}
							else if($value->id_usuario2 == $id)
							{
								if($value->activo == 3)
								{
									$activo3 = $activo3+1; 
									$amigo = Usuario::model()->findByPk($value->id_usuario1);
									$pendientes = $pendientes.
									'<li>
										<img src="'.$amigo->imagen.'" width="50px" height="50px">
										<span>'.$amigo->nombre.'</span>'.
										'<button onclick="window.location.href='."'".$linkPerfil.$amigo->id."'".'" type="button" class="btn btn-default" aria-label="Left Align">
											<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
										</button>'.
										'<button onclick="window.location.href='."'".$linkAcetpar.$amigo->nombre."'".'" type="button" class="btn btn-default" aria-label="Left Align">
											<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
										</button>'.
										'<button onclick="window.location.href='."'".$linkEliminar.$amigo->id."'".'" type="button" class="btn btn-default" aria-label="Left Align">
											<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
										</button>
									</li>';
								}else
								{
									$activo1 = $activo1+1;
									$amigo = Usuario::model()->findByPk($value->id_usuario1);
									$agregados = $agregados.
									'<li class="col-md-6 lista_amigos">
										<img src="'.$amigo->imagen.'" width="50px" height="50px">
										<span>'.$amigo->nombre.'</span>'.
										'<button onclick="window.location.href='."'".$linkPerfil.$amigo->id."'".'" type="button" class="btn btn-default" aria-label="Left Align">
											<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
										</button>'.
										'<button onclick="window.location.href='."'".$linkAcetpar.$amigo->nombre."'".'" type="button" class="btn btn-default" aria-label="Left Align">
											<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
										</button>'.
										'<button onclick="window.location.href='."'".$linkEliminar.$amigo->id."'".'" type="button" class="btn btn-default" aria-label="Left Align">
											<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
										</button>
									</li>';
								}
							}
					}

					$agregados = $agregados.'</ul>';
					$pendientes = $pendientes.'</ul>';
					$this->render('amigos',array('fotoUsuario'=>$fotoUsuario,'nombreUsuario'=>$nombreUsuario,'model'=>$model,'agregados'=>$agregados,'pendientes'=>$pendientes,'id'=>$id,'totalAmigos'=>$totalAmigos,'totalNoAmigos'=>$totalNoAmigos,'activo1'=>$activo1,'activo3'=>$activo3));
				}else
				{
					$activo1 = 0;
					$activo3 = 0;
					$this->render('amigos',array('fotoUsuario'=>$fotoUsuario,'nombreUsuario'=>$nombreUsuario,'model'=>$model,'totalAmigos'=>$totalAmigos,'totalNoAmigos'=>$totalNoAmigos,'activo1'=>$activo1,'activo3'=>$activo3));
				}
			}

		}else
		{
			$this->redirect($this->createUrl('site/login'));
		}
	}

	public function actionEliminarUsuario()
	{
		$id = Yii::app()->user->id;
		$model = new Usuario;

		if($id!=null)
		{	
		
			if(isset($_POST['Usuario']))
			{	
				$id = Yii::app()->user->id;
				$model->imagen = $_POST['Usuario']['foto'];
				$file = $_POST['Usuario']['foto'];
				$nombreFoto = $file;

				$trozos = explode(".", $nombreFoto); 
				$extension = end($trozos); 

				if($extension=='jpg' || $extension=='jpeg')
				{
					$ruta = "/comunidad/uploads";
					$resultado = @move_uploaded_file($_POST["Usuario"]["foto"], $ruta);
				}else
				{
					Yii::app()->user->setFlash('mensaje','La foto debe ser solo JPG/JPEG');
					$this->refresh();
				}

				$nombreFoto = '/comunidad/images/'.$nombreFoto;

				$query = Yii::app()->db->createCommand()
						->update('Usuario',
						array('imagen'=>new CDbExpression(':imagen', array(':imagen'=>$nombreFoto))),
						'id=:id', array(':id'=>$id)
						);

				if($query){
					$this->redirect($this->createUrl('comunidad/perfil'));
				}

			}else
			{
				if(isset($_GET['data']))
				{
					$idAmigo = $_GET['data'];
					$amigo = Usuario::model()->findByPk($idAmigo);
					$eliminarAmistad = Amigo::model()->find('id_usuario1='.$id.' && id_usuario2='.$idAmigo.' || id_usuario1='.$idAmigo.' && id_usuario2='.$id);
					$resultEliminarAmistad = $eliminarAmistad->delete(); 
					if($resultEliminarAmistad)
					{
						Yii::app()->user->setFlash('success', 'El usuario se ha eliminado correctamente');
					}else
					{
						Yii::app()->user->setFlash('error', 'No se ha podido eliminar al usuario');
					}

				}

				$fotoUsuario = Usuario::model()->findByPk($id)->imagen;
				$nombreUsuario = Usuario::model()->findByPk($id)->nombre;
				$amigosUsuario = Amigo::model()->findAll('id_usuario1='.$id.' || id_usuario2='.$id);
				$totalAmigosAgregados = Amigo::model()->findAll('(id_usuario1='.$id.' || id_usuario2='.$id.') && activo=1');
				$totalAmigos = sizeof($totalAmigosAgregados);
				$totalAmigosNoAgregados = Amigo::model()->findAll('(id_usuario1='.$id.' || id_usuario2='.$id.') && activo=3');
				$totalNoAmigos = sizeof($totalAmigosNoAgregados);

				if($amigosUsuario != null)
				{
					$agregados = '<ul>';
					$pendientes = '<ul>';
					$activo1 = 0;
					$activo3 = 0;
					$linkPerfil = "/comunidad/index.php/comunidad/perfilAmigo?idAmigo=";
					$linkAcetpar = "/comunidad/index.php/comunidad/aceptarUsuario?nombreUsuario=";
					$linkEliminar = "/comunidad/index.php/comunidad/eliminarUsuario?data=";

					foreach ($amigosUsuario as $key => $value) 
					{
						if($value->id_usuario1 == $id)
						{
							if($value->activo == 3)
							{
								$activo3 = $activo3+1; 
								$amigo = Usuario::model()->findByPk($value->id_usuario2);
								$pendientes = $pendientes.
								'<li>
									<img src="'.$amigo->imagen.'" width="50px" height="50px">
									<span>'.$amigo->nombre.'</span>'.
									'<button onclick="window.location.href='."'".$linkPerfil.$amigo->id."'".'" type="button" class="btn btn-default" aria-label="Left Align">
										<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
									</button>'.
									'<button onclick="window.location.href='."'".$linkAcetpar.$amigo->nombre."'".'" type="button" class="btn btn-default" aria-label="Left Align">
										<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
									</button>'.
									'<button onclick="window.location.href='."'".$linkEliminar.$amigo->id."'".'" type="button" class="btn btn-default" aria-label="Left Align">
										<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
									</button>
								</li>';
							}else
							{
								$activo1 = $activo1+1;
								$amigo = Usuario::model()->findByPk($value->id_usuario2);
								$agregados = $agregados.
								'<li class="col-md-6 lista_amigos">
									<img src="'.$amigo->imagen.'" width="50px" height="50px">
									<span>'.$amigo->nombre.'</span>'.
									'<button onclick="window.location.href='."'".$linkPerfil.$amigo->id."'".'" type="button" class="btn btn-default" aria-label="Left Align">
										<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
									</button>'.
									'<button onclick="window.location.href='."'".$linkAcetpar.$amigo->nombre."'".'" type="button" class="btn btn-default" aria-label="Left Align">
										<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
									</button>'.
									'<button onclick="window.location.href='."'".$linkEliminar.$amigo->id."'".'" type="button" class="btn btn-default" aria-label="Left Align">
										<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
									</button>
								</li>';
							}
						}
							else if($value->id_usuario2 == $id)
							{
								if($value->activo == 3)
								{
									$activo3 = $activo3+1; 
									$amigo = Usuario::model()->findByPk($value->id_usuario1);
									$pendientes = $pendientes.
									'<li>
										<img src="'.$amigo->imagen.'" width="50px" height="50px">
										<span>'.$amigo->nombre.'</span>'.
										'<button onclick="window.location.href='."'".$linkPerfil.$amigo->id."'".'" type="button" class="btn btn-default" aria-label="Left Align">
											<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
										</button>'.
										'<button onclick="window.location.href='."'".$linkAcetpar.$amigo->nombre."'".'" type="button" class="btn btn-default" aria-label="Left Align">
											<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
										</button>'.
										'<button onclick="window.location.href='."'".$linkEliminar.$amigo->id."'".'" type="button" class="btn btn-default" aria-label="Left Align">
											<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
										</button>
									</li>';
								}else
								{
									$activo1 = $activo1+1;
									$amigo = Usuario::model()->findByPk($value->id_usuario1);
									$agregados = $agregados.
									'<li class="col-md-6 lista_amigos">
										<img src="'.$amigo->imagen.'" width="50px" height="50px">
										<span>'.$amigo->nombre.'</span>'.
										'<button onclick="window.location.href='."'".$linkPerfil.$amigo->id."'".'" type="button" class="btn btn-default" aria-label="Left Align">
											<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
										</button>'.
										'<button onclick="window.location.href='."'".$linkAcetpar.$amigo->nombre."'".'" type="button" class="btn btn-default" aria-label="Left Align">
											<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
										</button>'.
										'<button onclick="window.location.href='."'".$linkEliminar.$amigo->id."'".'" type="button" class="btn btn-default" aria-label="Left Align">
											<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
										</button>
									</li>';
								}
							}
					}

					$agregados = $agregados.'</ul>';
					$pendientes = $pendientes.'</ul>';

					$this->render('amigos',array('fotoUsuario'=>$fotoUsuario,'nombreUsuario'=>$nombreUsuario,'model'=>$model,'agregados'=>$agregados,'pendientes'=>$pendientes,'id'=>$id,'totalAmigos'=>$totalAmigos,'totalNoAmigos'=>$totalNoAmigos,'activo1'=>$activo1,'activo3'=>$activo3));
				}else
				{
					$activo1 = 0;
					$activo3 = 0;
					$this->render('amigos',array('fotoUsuario'=>$fotoUsuario,'nombreUsuario'=>$nombreUsuario,'model'=>$model,'totalAmigos'=>$totalAmigos,'totalNoAmigos'=>$totalNoAmigos,'activo1'=>$activo1,'activo3'=>$activo3));
				}
			}

		}else
		{
			$this->redirect($this->createUrl('site/login'));
		}
	}

	public function actionCargarContactos()
	{
		$id = Yii::app()->user->id;
		$string= "<ul>";

		$amigos = Amigo::model()->findAll('id_usuario1='.$id.' || id_usuario2='.$id);
		
		foreach ($amigos as $key => $value) 
		{
			if($value->id_usuario1 == $id && $value->activo == 1)
			{
				$buscarAmigo = Usuario::model()->findByPk($value->id_usuario2);
				$string = $string."<li class='user' onclick='cargar(".$buscarAmigo->id.")'>";
				$string = $string.'<img class="img-circle" src="'.$buscarAmigo->imagen.'" width="50px" height="50px">';
				$string = $string.'<span class="valor">'.$buscarAmigo->nombre.'</span>';
				$string = $string."</li>";
			}
				else if($value->id_usuario2 == $id && $value->activo == 1)
				{
					$buscarAmigo = Usuario::model()->findByPk($value->id_usuario1);
					$string = $string."<li class='user' onclick='cargar(".$buscarAmigo->id.")'>";
					$string = $string.'<img class="img-circle" src="'.$buscarAmigo->imagen.'" width="50px" height="50px">';
					$string = $string.'<span class="valor">'.$buscarAmigo->nombre.'</span>';
					$string = $string."</li>";
				}
		}

		$string = $string."</ul>";

		echo json_encode($string);
	}

	public function actionCargarChatUsuario()
	{
		$id = Yii::app()->user->id;
		//$usuario = Usuario::model()->find('nombre='.'"'.$_POST['data'].'"');
		$usuario = Usuario::model()->findByPk($_POST['data']);

		if(isset($_POST['data']))
		{
			$mensajes = Mensaje::model()->findAll('id_usuario1='.$id.' && id_usuario2='.$usuario->id.' || id_usuario1='.$usuario->id.' && id_usuario2='.$id);

			if($mensajes != null)
			{
				$chat = '<div class="col-md-2 msg_box">';
				$chat = $chat.'<div class="msg_head"><p class="nombre">'.$usuario->nombre.'</p><div class="close">x</div></div>';
				$chat = $chat.'<div class="msg_wrap">';
				$chat = $chat.'<div class="msg_body">';

				foreach ($mensajes as $key => $value) {
					if($value->id_usuario1 == $id)
					{
						$chat = $chat.'<div class="msg_b">'.$value->texto.'</div>';
					}
						else if($value->id_usuario2 == $id)
						{
							$chat = $chat.'<div class="msg_a">'.$value->texto.'</div>';
						}
				}

				$chat = $chat.'<div class="msg_insert"></div>';
				$chat = $chat.'</div>';
				$chat = $chat.'<div class="msg_footer">
									<textarea class="msg_input" rows="2">sample</textarea>
							   </div>';
				$chat = $chat.'</div></div>';
			}else
			{
				$chat = '<div class="col-md-2 msg_box">';
				$chat = $chat.'<div class="msg_head">'.$usuario->nombre.'<div class="close">x</div></div>';
				$chat = $chat.'<div class="msg_wrap">';
				$chat = $chat.'<div class="msg_body">';
				$chat = $chat.'<div class="msg_insert"></div>';
				$chat = $chat.'</div>';
				$chat = $chat.'<div class="msg_footer">
									<textarea class="msg_input" rows="2">sample</textarea>
							   </div>';
				$chat = $chat.'</div></div>';
			}

			echo json_encode($chat);
		}
	}

	public function actionGuardarMensaje()
	{
		$id = Yii::app()->user->id;

		if(isset($_POST['data']))
		{
		 	$mensaje = $_POST['data'];
		 	$nombre = $_POST['nombre'];
		 	$usuario = Usuario::model()->find('nombre="'.$nombre.'"')->id;
		 	$model = new Mensaje;
		 	$model->texto = $mensaje;
		 	$model->id_usuario1 = $id;
		 	$model->id_usuario2 = $usuario;
		 	$model->save();
		}
	}

}