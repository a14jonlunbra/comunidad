<?php

class ValidarRegistro extends CFormModel
{
	public $nombre;
	public $email;
	public $password;
	public $repetir_password;
	public $terminos;
	public $captcha;

	public function rules()
	{
		return array
		(
			array(
				'nombre',
				'required',
				'message'=>'Este campo es requerido'
			),
			//array('nombre','match','pattern'=>'/^[a-zA-Z\s]+$/', 'message'=>'El tipo de datos introducido es incorrecto'),
			array(
				'nombre',
				'length',
				'min'=>5,
				'tooShort'=>'El nombre debe tener como mínimo 5 caracteres',
				'max'=>30,
				'tooLong'=>'Máximo 50 caracteres'
			),
			array(
				'nombre','comprobar_nombre',
			),
			/* Validacion de Email */
			array(
				'email',
				'required',
				'message'=>'El email es requerido',
			),
			array(
				'email',
				'email', 'message'=>'El formato de email introducido no es correcto',
			),
			array(
				'email',
				'length',
				'min'=>8,
				'tooShort'=>'El email debe tener como mínimo 8 carácteres',
				'max'=>70,
				'tooLong'=>'El email debe tener como máximo 8 carácteres',
			),
			array(
				'email','comprobar_email',
			),
			/* Validacion de password */
			array(
				'password',
				'required',
				'message'=>'Este campo es requerido',
			),
			array(
				'password',
				'match',
				'pattern'=>'/^([a-z]+[0-9]+)|([0-9]+[a-z]+)/i',
				'message'=>'Es obligatorio que la password contenga letras y números',
			),
			array(
				'password',
				'length',
				'min'=>8,
				'tooShort'=>'La contraseña debe tener mínimo 8 carácteres',
				'max'=>16,
				'tooLong'=>'La contraseña debe tener como máximo 16 carácteres',
			),
			/* Validacion de repetir_password */
			array(
				'repetir_password',
				'required',
				'message'=>'Este campo es requerido',
			),
			array(
				'repetir_password',
				'compare',
				'compareAttribute'=>'password',
				'message'=>'Las contraseñas no coinciden',
			),
			/* Validar checkbox */
			array(
				'terminos',
				'required',
				'message'=>'Por favor, acepte los terminos',
			),
			/* Validar Captcha */
			array(
				'captcha',
				'captcha',
				'message'=>'Error el texto no es correcto',
			),
		); 
	}

	/* Cambiar el nombre de los atributos del formulario */
	public function attributeLabels()
	{
		return array(
			'terminos'=>'Términos y condiciones',
		);
	}

	/* Crear nuevas opciones de validacion */
	public function comprobar_email()
	{
		$emailUsuarios = Usuario::model()->findAll();

		foreach ($emailUsuarios as $key => $value) {
			 if($this->email == $value->email)
			 {
			 	$this->addError('email','Email no disponible');
			 	break;
			 }
		}

	}

	public function comprobar_nombre()
	{
		$nombreUsuarios = Usuario::model()->findAll();

		foreach ($nombreUsuarios as $key => $value) {
			 if($this->nombre == $value->nombre)
			 {
			 	$this->addError('nombre','Nombre no disponible');
			 	break;
			 }
		}

	}

}

?>